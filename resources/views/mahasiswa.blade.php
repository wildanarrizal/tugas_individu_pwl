@extends('template/template')

@section('title', 'Mahasiswa')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10">
                <h1 class="mt-3">Daftar Mahasiswa UB</h1>
                @foreach ($data as $item)
                    {{ $item }}<br />
                @endforeach
            @endsection
        </div>
    </div>
</div>
